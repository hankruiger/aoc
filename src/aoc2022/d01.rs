use std::{
  fs::File,
  io::{BufRead, BufReader},
};

pub fn main() {
  let input = File::open("data/2022/day/1/input").expect("could not find input data");
  let reader = BufReader::new(input);

  let lines: Vec<String> = reader.lines().map(|line| line.unwrap()).collect();

  let mut calories_per_elf: Vec<usize> = Vec::new();
  let mut acc = 0;
  for line in lines {
    match line.parse::<usize>() {
      Ok(cals) => acc += cals,
      Err(_) => {
        calories_per_elf.push(acc);
        acc = 0;
      }
    };
  }
  calories_per_elf.push(acc);

  calories_per_elf.sort();
  calories_per_elf.reverse();

  println!(
    "The elf carrying the most calories was carrying {} calories. Strong elf!",
    calories_per_elf[0]
  );

  println!(
    "The runner up elves carry {} and {} calories. Mildly strong elves!",
    calories_per_elf[1], calories_per_elf[2]
  );

  println!(
    "Together, the top 3 elves carry {} calories.",
    calories_per_elf[..3].iter().sum::<usize>()
  )
}
