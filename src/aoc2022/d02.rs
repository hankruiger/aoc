use std::{
  fs::File,
  io::{BufRead, BufReader},
  str::FromStr,
};

pub fn main() {
  let input = File::open("data/2022/day/2/input").expect("could not find input data");
  let reader = BufReader::new(input);

  let total: usize = reader
    .lines()
    .map(|line| line.unwrap())
    .map(|line| line.parse::<Round>().unwrap())
    .map(|round| round.points())
    .sum();

  println!(
    "With this strategy, your total points would be {}. Not bad! Or is it?",
    total
  );

  let input2 = File::open("data/2022/day/2/input").expect("could not find input data");
  let reader2 = BufReader::new(input2);
  let total2: usize = reader2
    .lines()
    .map(|line| line.unwrap())
    .map(|line| line.parse::<RoundFixed>().unwrap())
    .map(|round_fix| Into::<Round>::into(round_fix))
    .map(|round| round.points())
    .sum();
  println!(
    "If you fix the rounds in the agreed manner, you will get {} points. Tempting...!",
    total2
  );
}

#[derive(PartialEq, Eq, Copy, Clone)]
#[repr(usize)]
enum Sign {
  ROCK,
  PAPER,
  SCISSORS,
}

impl Sign {
  pub fn base_points(&self) -> usize {
    match self {
      Sign::ROCK => 1,
      Sign::PAPER => 2,
      Sign::SCISSORS => 3,
    }
  }
}

impl From<i32> for Sign {
  fn from(n: i32) -> Self {
    match n {
      0 => Sign::ROCK,
      1 => Sign::PAPER,
      2 => Sign::SCISSORS,
      _ => panic!("Invalid Sign value: {}", n),
    }
  }
}

struct Round {
  opponent: Sign,
  you: Sign,
}

impl Round {
  pub fn points(&self) -> usize {
    let victory_points = match (self.you as i32 - self.opponent as i32).rem_euclid(3) {
      0 => 3, // they're equal -> tie
      1 => 6, // `you` is 1 'ahead' so it beats `opponent` -> win
      2 => 0, // `you` is 2 'ahead' (so 1 'behind') so `opponent` beats you -> lose
      something_else => panic!("{} should not be possible", something_else),
    };
    self.you.base_points() + victory_points
  }
}

impl FromStr for Round {
  type Err = String;

  fn from_str(s: &str) -> Result<Self, Self::Err> {
    let opponent = match s.chars().nth(0) {
      Some('A') => Sign::ROCK,
      Some('B') => Sign::PAPER,
      Some('C') => Sign::SCISSORS,
      _ => panic!("Invalid `opponent` sign"),
    };

    let you = match s.chars().nth(2) {
      Some('X') => Sign::ROCK,
      Some('Y') => Sign::PAPER,
      Some('Z') => Sign::SCISSORS,
      _ => panic!("Invalid `you` sign"),
    };

    Ok(Round { opponent, you })
  }
}

enum FixGoal {
  LOSE,
  TIE,
  WIN,
}

struct RoundFixed {
  opponent: Sign,
  goal: FixGoal,
}

impl FromStr for RoundFixed {
  type Err = String;

  fn from_str(s: &str) -> Result<Self, Self::Err> {
    let opponent = match s.chars().nth(0) {
      Some('A') => Sign::ROCK,
      Some('B') => Sign::PAPER,
      Some('C') => Sign::SCISSORS,
      _ => panic!("Invalid `opponent` sign"),
    };

    let goal = match s.chars().nth(2) {
      Some('X') => FixGoal::LOSE,
      Some('Y') => FixGoal::TIE,
      Some('Z') => FixGoal::WIN,
      _ => panic!("Invalid `goal` sign"),
    };

    Ok(RoundFixed { opponent, goal })
  }
}

impl Into<Round> for RoundFixed {
  fn into(self) -> Round {
    let you = match self.goal {
      FixGoal::TIE => self.opponent,
      FixGoal::WIN => Sign::from(((self.opponent as i32) + 1).rem_euclid(3)),
      FixGoal::LOSE => Sign::from(((self.opponent as i32) - 1).rem_euclid(3)),
    };
    Round {
      opponent: self.opponent,
      you,
    }
  }
}
