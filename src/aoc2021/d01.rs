use std::{
  collections::VecDeque,
  fs::File,
  io::{BufRead, BufReader},
};

pub fn main() {
  // Part 1: Process the raw measurements
  let input = File::open("data/2021/day/1/input").expect("could not find input data");
  let reader = BufReader::new(input);

  let measurements = reader.lines().map(|l| l.unwrap().parse().unwrap());

  println!("{}", count_increments(measurements));

  // Part 2: Process the sliding window sums of the measurements
  let input = File::open("data/2021/day/1/input").expect("could not find input data");
  let reader = BufReader::new(input);

  let measurements = reader.lines().map(|l| l.unwrap().parse().unwrap());

  let sliding_sums = SlidingWindow::new(3, Box::new(measurements.into_iter()));

  println!("{}", count_increments(sliding_sums));
}

fn count_increments<I>(values: I) -> u32
where
  I: IntoIterator<Item = u32>,
{
  let mut previous: Option<u32> = None;
  let mut increments = 0u32;
  for current in values {
    if let Some(previous) = previous {
      if current > previous {
        increments += 1;
      }
    }
    previous = Some(current);
  }
  increments
}

struct SlidingWindow {
  source: Box<dyn Iterator<Item = u32>>,
  window_size: usize,
  window: VecDeque<u32>,
}

impl SlidingWindow {
  pub fn new(window_size: usize, source: Box<dyn Iterator<Item = u32>>) -> Self {
    SlidingWindow {
      window_size,
      window: VecDeque::with_capacity(window_size),
      source,
    }
  }
}

impl Iterator for SlidingWindow {
  type Item = u32;

  fn next(&mut self) -> Option<Self::Item> {
    // First make sure the queue is full before yielding values.
    while self.window.len() < self.window_size {
      match self.source.next() {
        Some(raw_value) => self.window.push_back(raw_value),
        // If the source is exhausted we cannot yield values anymore.
        None => return None,
      }
    }
    let sum: u32 = self.window.iter().sum();

    // Remove the first element, we don't need it any longer; the window is
    // sliding away from it.
    self.window.pop_front();

    Some(sum)
  }
}

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn example_part_1() {
    assert_eq!(
      count_increments(vec![199, 200, 208, 210, 200, 207, 240, 269, 260, 263]),
      7
    );
  }

  #[test]
  fn example_part_2() {
    let sliding_window = SlidingWindow::new(
      3,
      Box::new(vec![199, 200, 208, 210, 200, 207, 240, 269, 260, 263].into_iter()),
    );

    let sliding_sums: Vec<u32> = sliding_window.collect();

    assert_eq!(sliding_sums, vec![607, 618, 618, 617, 647, 716, 769, 792]);

    assert_eq!(count_increments(sliding_sums), 5)
  }

  #[test]
  fn sliding_window() {
    let window = SlidingWindow::new(2, Box::new(vec![1u32, 1u32, 1u32, 1u32].into_iter()));
    assert_eq!(window.collect::<Vec<u32>>(), vec![2, 2, 2]);
  }
}
