use std::{
  fs::File,
  io::{BufRead, BufReader},
};

pub fn main() {
  // Part 1: Finding gamma and epsilon
  let input = File::open("data/2021/day/3/input").expect("could not find input data");
  let reader = BufReader::new(input);

  let report = reader.lines().map(|l| l.unwrap());

  let [gamma, epsilon] = find_gamma_epsilon_rates(report);
  println!("{}", gamma * epsilon);

  // Part 2: Finding O2 generator rating and CO2 scrubber rating.
  let input = File::open("data/2021/day/3/input").expect("could not find input data");
  let reader = BufReader::new(input);

  let report = reader.lines().map(|l| l.unwrap());
  let [o2_generator_rating, co2_scrubbing_rating] =
    find_o2_generator_and_co2_scrubbing_rating(report);
  println!("{}", o2_generator_rating * co2_scrubbing_rating);
}

pub fn find_gamma_epsilon_rates<I>(report: I) -> [usize; 2]
where
  I: IntoIterator<Item = String>,
{
  let mut counts: Option<Vec<usize>> = None;
  let mut total_count = 0usize;
  let mut n_bits = None;
  for number in report {
    // From the first number we get the number of bits in the report.
    if counts.is_none() {
      n_bits = Some(number.len());
      counts = Some(vec![0; n_bits.unwrap()]);
    }
    for (bit_number, bit) in number.chars().enumerate() {
      if bit == '1' {
        if let Some(counts) = counts.as_mut() {
          counts[bit_number] += 1;
        }
      }
    }
    total_count += 1;
  }

  // We can unwrap these now; they are known to be defined.
  let n_bits = n_bits.unwrap();
  let counts = counts.unwrap();

  let mut gamma = 0;
  for (bit_number, &count) in counts.iter().enumerate() {
    if count > total_count / 2 {
      gamma += 1 << (n_bits - 1 - bit_number);
    }
  }

  // This many bits are unused in our representation (the strings are shorter
  // than 64, hopefully..)
  let unused_bits = usize::BITS as usize - n_bits;

  // Get the epsilon by inverting the gamma, and then turning all the 1s that
  // were in the unused region of the usize into 0s by bitshifting them left and
  // right.
  let epsilon = (!gamma << unused_bits) >> unused_bits;

  return [gamma, epsilon];
}

pub fn find_o2_generator_and_co2_scrubbing_rating<I>(report: I) -> [usize; 2]
where
  I: IntoIterator<Item = String>,
{
  let report: Vec<String> = report.into_iter().collect();
  let o2_generator_rating;
  let co2_scrubbing_rating;
  {
    let mut report = report.clone();
    let mut bit_index = 0;
    while report.len() > 1 {
      let most_common = find_most_or_least_common_bit(&report, bit_index, true);

      // Keep only the values in the report that have the most common bit at index `bit_index`
      report = report
        .into_iter()
        .filter(|n| n.chars().nth(bit_index).unwrap() == most_common)
        .collect();

      bit_index += 1;
    }

    if report.len() == 1 {
      o2_generator_rating = Some(usize::from_str_radix(report[0].as_str(), 2).unwrap());
    } else {
      panic!("no values left in report for O2 generator rating!");
    }
  }

  {
    let mut report = report.clone();
    let mut bit_index = 0;
    while report.len() > 1 {
      let least_common = find_most_or_least_common_bit(&report, bit_index, false);

      // Keep only the values in the report that have the least common bit at index `bit_index`
      report = report
        .into_iter()
        .filter(|n| n.chars().nth(bit_index).unwrap() == least_common)
        .collect();

      bit_index += 1;
    }

    if report.len() == 1 {
      co2_scrubbing_rating = Some(usize::from_str_radix(report[0].as_str(), 2).unwrap());
    } else {
      panic!("no values left in report for CO2 scrubbing rating!");
    }
  }

  return [o2_generator_rating.unwrap(), co2_scrubbing_rating.unwrap()];
}

fn find_most_or_least_common_bit(report: &Vec<String>, bit: usize, most_common: bool) -> char {
  let mut zeros = 0;
  let mut ones = 0;
  for value in report {
    let char = value.chars().nth(bit).unwrap();
    if char == '0' {
      zeros += 1;
    } else if char == '1' {
      ones += 1;
    } else {
      panic!("unexpected char {}", char);
    }
  }

  if most_common {
    if zeros > ones {
      '0'
    } else {
      '1'
    }
  } else {
    if ones < zeros {
      '1'
    } else {
      '0'
    }
  }
}

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn example_1() {
    assert_eq!(
      find_gamma_epsilon_rates(
        vec![
          "00100", "11110", "10110", "10111", "10101", "01111", "00111", "11100", "10000", "11001",
          "00010", "01010",
        ]
        .into_iter()
        .map(|n| String::from(n))
      ),
      [22, 9]
    )
  }

  #[test]
  fn example_2() {
    assert_eq!(
      find_o2_generator_and_co2_scrubbing_rating(
        vec![
          "00100", "11110", "10110", "10111", "10101", "01111", "00111", "11100", "10000", "11001",
          "00010", "01010",
        ]
        .into_iter()
        .map(|n| String::from(n))
      ),
      [23, 10]
    )
  }
}
