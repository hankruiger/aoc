use std::{
  fs::File,
  io::{BufRead, BufReader},
};

pub fn main() {
  // part 1, where the commands are interpreted incorrectly
  let input = File::open("data/2021/day/2/input").expect("could not find input data");
  let reader = BufReader::new(input);

  // parse the commands
  let commands = reader
    .lines()
    .map(|l| SubmarineCommand::try_from(l.unwrap()).unwrap());

  // let the submarine interpret them
  let mut submarine = Submarine::new();
  for cmd in commands {
    submarine.interpret_wrong(&cmd);
  }

  println!("{}", submarine.horizontal_position * submarine.depth);

  // part 2, where the commands are interpreted correctly
  let input = File::open("data/2021/day/2/input").expect("could not find input data");
  let reader = BufReader::new(input);

  // parse the commands
  let commands = reader
    .lines()
    .map(|l| SubmarineCommand::try_from(l.unwrap()).unwrap());

  // let the submarine interpret them, correctly this time
  let mut submarine = Submarine::new();
  for cmd in commands {
    submarine.interpret_more_complicated(&cmd);
  }

  println!("{}", submarine.horizontal_position * submarine.depth);
}

#[derive(Debug, Eq, PartialEq)]
enum SubmarineCommand {
  Forward(i32),
  Down(i32),
  Up(i32),
}

impl TryFrom<String> for SubmarineCommand {
  type Error = &'static str;

  fn try_from(value: String) -> Result<Self, Self::Error> {
    let parts: Vec<&str> = value.split(" ").collect();
    if parts.len() != 2 {
      return Err(
        "a SubmarineCommand must have two parts separated by a space: \"[direction] [magnitude]\"",
      );
    }

    let direction_input = parts[0];
    let magnitude_input = parts[1];

    let magnitude = match magnitude_input.parse::<i32>() {
      Ok(m) => m,
      Err(_) => {
        return Err("[magnitude] must be a valid decimal number");
      }
    };

    if direction_input == "forward" {
      Ok(SubmarineCommand::Forward(magnitude))
    } else if direction_input == "down" {
      Ok(SubmarineCommand::Down(magnitude))
    } else if direction_input == "up" {
      Ok(SubmarineCommand::Up(magnitude))
    } else {
      Err("[direction] must be \"forward\", \"down\", or \"up\"")
    }
  }
}

struct Submarine {
  horizontal_position: i32,
  depth: i32,
  aim: i32,
}

impl Submarine {
  pub fn new() -> Self {
    Submarine {
      horizontal_position: 0,
      depth: 0,
      aim: 0,
    }
  }
  pub fn interpret_wrong(&mut self, cmd: &SubmarineCommand) {
    match cmd {
      SubmarineCommand::Forward(n) => {
        self.horizontal_position += n;
      }
      SubmarineCommand::Down(n) => {
        self.depth += n;
      }
      SubmarineCommand::Up(n) => {
        self.depth -= n;
      }
    }
  }

  pub fn interpret_more_complicated(&mut self, cmd: &SubmarineCommand) {
    match cmd {
      SubmarineCommand::Forward(n) => {
        self.horizontal_position += n;
        self.depth += self.aim * n;
      }
      SubmarineCommand::Down(n) => {
        self.aim += n;
      }
      SubmarineCommand::Up(n) => {
        self.aim -= n;
      }
    }
  }
}

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn example_1() {
    let cmd_strings = vec![
      "forward 5",
      "down 5",
      "forward 8",
      "up 3",
      "down 8",
      "forward 2",
    ];
    let cmds = cmd_strings
      .into_iter()
      .map(|cmd_str| SubmarineCommand::try_from(cmd_str.to_string()).unwrap());

    let mut submarine = Submarine::new();
    for cmd in cmds {
      submarine.interpret_wrong(&cmd);
    }

    assert_eq!(submarine.horizontal_position, 15);
    assert_eq!(submarine.depth, 10);
  }

  #[test]
  fn example_2() {
    let cmd_strings = vec![
      "forward 5",
      "down 5",
      "forward 8",
      "up 3",
      "down 8",
      "forward 2",
    ];
    let cmds = cmd_strings
      .into_iter()
      .map(|cmd_str| SubmarineCommand::try_from(cmd_str.to_string()).unwrap());

    let mut submarine = Submarine::new();
    for cmd in cmds {
      submarine.interpret_more_complicated(&cmd);
    }

    assert_eq!(submarine.horizontal_position, 15);
    assert_eq!(submarine.depth, 60);
  }

  #[test]
  fn parse_command() {
    assert_eq!(
      SubmarineCommand::try_from(String::from("up 4")).unwrap(),
      SubmarineCommand::Up(4)
    );
    assert_eq!(
      SubmarineCommand::try_from(String::from("down 6")).unwrap(),
      SubmarineCommand::Down(6)
    );
  }

  #[test]
  fn interpret_command_wrong() {
    let mut submarine = Submarine::new();
    assert_eq!(submarine.horizontal_position, 0);
    assert_eq!(submarine.depth, 0);

    submarine.interpret_wrong(&SubmarineCommand::Down(42));

    assert_eq!(submarine.horizontal_position, 0);
    assert_eq!(submarine.depth, 42);
  }
}
